# Agenda 2018
Please upload your **slides** or a **introduction (Chinese or English)** of your presentation **in advance**,
such as **conference, title, abstract**，which can be written in the form of [markdown](http://sspai.com/25137). Please add your title in the agenda.

**Location**： Caoguangbiao-201

**Time**: 18:00-20:00  Welcome to attend the seminar!

||Date|Speaker|Title|
|---|:---:|:---:|:---:|
|1|2018.1.5|施程辉 <br> 唐凯宇|DANCin SEQ2SEQ: Fooling Text Classifiers with Adversarial Text Example Generation <br>ADVERSARIAL EXAMPLES IN THE PHYSICAL WORLD|
|2|2018.1.12|徐晓刚 <br> 王博|Adversarial Attacks on Neural Network Policies <br> Meltdown & Spectre|
|3|2018.1.19|刘沛宇 <br> 陈源|Broken Fingers: On the Usage of the Fingerprint API in Android <br> Didn’t You Hear Me? — Towards More Successful Web Vulnerability Notifications |
|4|2018.1.26|卢令令 <br> 李长江|BLOCKBENCH: A Framework for Analyzing Private Blockchains <br>  Feature Squeezing:Detecting Adversarial Examples in Deep Neural Networks|
|5|2018.2.2|杜天宇 <br> 李进锋|Trojaning Attack on Neural Networks<br> Semi-supervised Knowledge Transfer for Deep Learning from Private Training Data|
|6|2018.2.9| <br> | Report
|7|2018.2.24|李宇薇 <br> 刘倩君| Neural Network-based Graph Embedding for Cross-Platform Binary Code Similarity Detection(CCS-17).
|8|2018.3.2|凌祥 <br>李旭嵘 |Adversarial Example Defenses: Ensembles of Weak Defenses are not Strong <br>WHITENING BLACK-BOX NEURAL NETWORKS
|9|2018.3.9 |伍一鸣 <br>刘倩君| Game of Missuggestions: Semantic Analysis of Search-Autocomplete Manipulations<br>When coding style survives compilation: de-anonymizing programmers from executable binaries.
|10|2018.3.16|翁海琴 <br> 贺思睿|<br>Machine Learning Models that Remember Too Much
|11|2018.3.23|周骏丰 <br> 魏成坤| VulDeePecker: A Deep Learning-Based System for Vulnerability Detection<br>IKP: Turning a PKI Around with Decentralized Automated Incentives
|12|2018.3.30|唐凯宇<br>吕晨阳|Knock Knock, Who’s There? Membership Inference on Aggregate Location Data<br>
|13|2018.4.6| 段辅正<br>陈源|How to Learn Klingon Without Dictionary: Detection and Measurement of Black Keywords Used by Underground Economy
|14|2018.4.13|肖特嗣<br>刘栩威| ZOO: Zeroth Order Optimization Based Black-box Attacks to Deep Neural Networks without Training Substitute Models <br>Predicting the resilience of obfuscated code against symbolic execution attacks via machine learning
|15|2018.4.20|周安妮<br>付丽嫆|PlatPal: Detecting Malicious Documents with Platform Diversity <br>BinSim: Trace-based Semantic Binary Diffing via System Call Sliced Segment Equivalence Checking
|16|2018.4.27|冯依南<br>刘丁豪|RELIABLE ATTACKS AGAINST BLACK-BOX MACHINE LEARNING MODEL<br>Town Crier: An Authenticated Data Feed for Smart Contracts
|17|2018.5.4|王博<br>李长江|
|18|2018.5.11|No Group meeting| 
|19|2018.5.18|刘沛宇| Prudent Practices for Designing Malware Experiments: Status Quo and Outlook
|20|2018.5.25|李宇薇| Angora: Efficient Fuzzing by Principled Search
|21|2018.6.1|刘倩君<br> 魏成坤|
|22|2018.6.8| 陈源<br> 吕晨阳| <br> CCS 2017：  Directed Greybox Fuzzing
|23|2018.6.15|王博<br>唐凯宇|
|24|2018.6.22|杜天宇<br>李进峰|
|25|2018.6.29|施程辉<br>贺思睿|
|26|2018.7.6|王琴应<br>宋宇|
|27|2018.7.13|董剑辉<br>|
|28|2018.7.20|吴含露<br>黄铮杰|



